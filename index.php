<?php
session_start();  // Запуск сессии
$currency = [
    'uah' => [
        'name' => 'Гривна',
        'course' => 1
    ],
    'usd' => [
        'name' => 'Доллар',
        'course' => 27.1
    ],
    'eur' => [
        'name' => 'Евро',
        'course' => 30.2
    ] 
    ];
if(!empty($_SESSION['data'])){  // Проверяем сессию на наличие данных в ней
    $choice = (float)$_SESSION['data']; // Приводим значение к float, для корректного вычисления и избежания ошибок типов
}else{
    $choice = $currency['uah']['course'];             // Сохраняем гривну по умолчанию, если ничего не выбрано!!!
}
$goods = [
    [
        'title' => 'Фисташки жареные',
        'price_val' => 46
    ],
    [
        'title' => 'Кешью жареный',
        'price_val' => 48
    ],
    [
        'title' => 'Арахис сырой',
        'price_val' => 36
    ],
    [
        'title' => 'Бразильский орех жареный',
        'price_val' => 55.5
    ],
    [
        'title' => 'Миндаль золотой сырой',
        'price_val' => 37.7
    ],
    [
        'title' => 'Черный тмин',
        'price_val' => 24
    ],
    [
        'title' => 'Фундук в скорлупе',
        'price_val' => 90.4
    ],
    [
        'title' => 'Грецкий орех очищенный',
        'price_val' => 19.35
    ],
    [
        'title' => 'Инжир вяленый',
        'price_val' => 99
    ],
    [
        'title' => 'Банановые чипсы',
        'price_val' => 124.65
    ]
];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №7 Гурца Алексея</title>
    <meta name="description" content="Работа с сессиями">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
            <div class="text-center">
                <h1>Домашнее задание №7</h1>
                <h2>Работа с сессиями. Вывод данных в таблицы с сохранением в сессию</h2>
            </div>
        </header>
    </div>
    <div class="container mb-2">
        <div class="row">
            <form action="currency_choice.php" method="POST">
                <div class="mb-3">
                    <label>
                        Выбор валюты
                        <select class="form-select" name="choice_value">
                            <option selected disabled>
                                <?php switch($_SESSION['data']): case $currency['usd']['course']:
                                    echo 'Выбранная валюта: ' . $currency['usd']['name'];
                                    break;
                                case $currency['eur']['course']:
                                    echo 'Выбранная валюта: ' . $currency['eur']['name'];
                                    break;
                                case $currency['uah']['course']:
                                    echo 'Выбранная валюта: ' . $currency['uah']['name'];
                                    break;
                                default:
                                    echo 'Валюта по умолчанию: ' . $currency['uah']['name'];
                                endswitch; ?>
                            </option>
                            <option value="<?=$currency['usd']['course']; // Записываем курс выбранной валюты по ключам массива?>">USD</option>
                            <option value="<?=$currency['eur']['course'];?>">EUR</option>
                            <option value="<?=$currency['uah']['course'];?>">UAH</option>
                        </select>
                    </label>
                </div>
                <button type="submit" class="btn btn-dark">Принять</button>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <form action="delete_choice.php" method="POST">
                <div class="mb-3">
                    <input type="hidden" name="deldata">
                    <button class="btn btn-danger">Сброс данных</button>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="">
            <table class="table table-hover">
                <thead class="table-primary">
                    <tr style="text-align: center;">
                    <th scope="col">Наименование товара</th>
                    <th scope="col">Цена товара в выбранной валюте</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($goods as $item): // Вывод таблицы по массиву ?>
                        <tr style="text-align: center;">
                            <td class="table-secondary"><?=$item['title'];?></td>
                            <td class="table-success"><?=round($item['price_val'] / $choice, 2); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container-fluid">
        <footer class="d-flex justify-content-center p-1 m-1 text-warning" style="background-color: #4b6477;">
            <p>Гурец Алексей &copy;2021 <a href="mailto:oleksii.gurets@gmail.com" class="text-warning">Все вопросы по
                    почте</a> <a href="/" class="text-warning">Главная</a></p>
        </footer>
    </div>
</body>

</html>